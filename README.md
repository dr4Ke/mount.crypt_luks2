# Description

This is a helper that enables LUKS2 volumes support for pam_mount.

# Installation
Just copy the files to `/usr/local/bin/` directory.

# Usage
In pam_mount.conf.xml, use something like this:
```
<volume user="myuser"
        fstype="crypt_LUKS2"
        path="/dev/vghomes/home%(USER)"
        mountpoint="~"
/>
```

## PAM
The instructions from the archlinux wiki work fine:
https://wiki.archlinux.org/index.php/Pam_mount#Login_manager_configuration

# Archlinux
For archlinux users, the `pam_mount-git` package should work:
https://aur.archlinux.org/packages/pam_mount-git/
